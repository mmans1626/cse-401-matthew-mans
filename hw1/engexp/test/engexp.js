///<reference path="../typings/main.d.ts"/>
"use strict";
var chai_1 = require("chai");
var engexp_1 = require("../src/engexp");
describe("EngExp", function () {
  /*----PROVIDED TESTS--*/
  it("should capture nested groups and work with disjunctions", () => {
    let e = new engexp_1['default']()
    .startOfLine()
    .then("https")
    .or("http")
    .then("://")
    .maybe("www.")
    .beginCapture()
    .beginCapture()
    .anythingBut("/")
    .endCapture()
    .anythingBut(" ")
    .endCapture()
    .endOfLine()
    .asRegExp();
    let result = e.exec("https://www.google.com/maps");
    chai_1.expect(result[1]).to.be.equal("google.com/maps");
  });
  it("should separate alternatives", () => {
    let e = new engexp_1['default']()
    .startOfLine()
    .digit().or("a")
    .then(" ")
    .then(new engexp_1['default']().digit().or("b"))
    .or("z").endOfLine().asRegExp();
    chai_1.expect(e.test("b z"), "should not match 'b z'").to.be.false;
  });
  it("should parse a basic URL", function () {
      var e = new engexp_1["default"]()
          .startOfLine()
          .then("http")
          .maybe("s")
          .then("://")
          .maybe("www.")
          .anythingBut(" ")
          .endOfLine()
          .asRegExp();
      chai_1.expect(e.test("https://www.google.com/maps")).to.be.true;
  });
  it("should parse a disjunctive date pattern", function () {
      var e = new engexp_1["default"]()
          .startOfLine()
          .digit().repeated(1, 2)
          .then("/")
          .then(new engexp_1["default"]().digit().repeated(1, 2))
          .then("/")
          .then(new engexp_1["default"]().digit().repeated(2, 4))
          .or(new engexp_1["default"]()
          .digit().repeated(1, 2)
          .then(" ")
          .then(new engexp_1["default"]().match("Jan").or("Feb").or("Mar").or("Apr").or("May").or("Jun")
          .or("Jul").or("Aug").or("Sep").or("Oct").or("Nov").or("Dec"))
          .then(" ")
          .then(new engexp_1["default"]().digit().repeated(2, 4)))
          .endOfLine()
          .asRegExp();
      chai_1.expect(e.test("12/25/2015")).to.be.true;
      chai_1.expect(e.test("25 Dec 2015")).to.be.true;
  });
  it("should capture nested groups", function () {
      var e = new engexp_1["default"]()
          .startOfLine()
          .then("http")
          .maybe("s")
          .then("://")
          .maybe("www.")
          .beginCapture()
          .beginCapture()
          .anythingBut("/")
          .endCapture()
          .anythingBut(" ")
          .endCapture()
          .endOfLine()
          .asRegExp();
      var result = e.exec("https://www.google.com/maps");
      chai_1.expect(result[1]).to.be.equal("google.com/maps");
      chai_1.expect(result[2]).to.be.equal("google.com");
  });

  /*--USER TESTS--*/
  it("should capture nested groups with a missing closing capture bracket", function () {
    var e = new engexp_1['default']()
      .startOfLine()
      .then("http")
      .maybe("s")
      .then("://")
      .maybe("www.")
      .beginCapture()
      .beginCapture()
      .anythingBut("/")
      .endCapture()
      .anythingBut(" ")
      .endOfLine()
      .asRegExp();
      
  var result = e.exec("https://www.google.com/maps");
  chai_1.expect(result[1]).to.be.equal("google.com/maps");
  chai_1.expect(result[2]).to.be.equal("google.com");
  });
  it("Should match one or more with a capture group but forget to close capture bracket", function() {
    var e = new engexp_1['default']()
      .match('Hello')
      .beginCapture()
      .match('World')
      .oneOrMore()
      .match('Fubar')
      .asRegExp();
    var result = e.exec("HelloWorldWorldFubarFoo");
    chai_1.expect(result[0]).to.be.equal('HelloWorldWorldFubar');
  });
  it("Should match one or more within a capture group", function() {
    var e = new engexp_1['default']()
      .match('Hello')
      .beginCapture()
      .match('World')
      .oneOrMore()
      .endCapture()
      .beginCapture()
      .match('Fubar')
      .endCapture()
      .match('Fubar')
      .asRegExp();
    var result = e.exec("HelloWorldFubarFubarFoo");
    chai_1.expect(result[0]).to.be.equal('HelloWorldFubarFubar');
    chai_1.expect(result[1]).to.be.equal('World');
    chai_1.expect(result[2]).to.be.equal('Fubar');
  });
  it("Should match with a missing closing capture bracket from passing in a new regex", function(){
    var e = new engexp_1['default']()
      .match('Hello')
      .match('World')
      .or(
        new engexp_1['default']()
        .match('Goodbye')
        .beginCapture()
        .maybe('Pluto')
        .match('Uranus')
      )
      .match("Barrr")
      .asRegExp();
      var result  = e.exec('FooWorldGoodbyePlutoUranusBarrr');
      chai_1.expect(result[0]).to.be.equal('GoodbyePlutoUranusBarrr');
      chai_1.expect(result[1]).to.be.equal('PlutoUranus');
  });
  it("Should match with capture group with missing an open capture bracket", function(){

    var e = new engexp_1["default"]()
      .match('Foo')
      .or('Bar')
      .endCapture()
      .asRegExp();
    var result = e.exec("FooBar");
    chai_1.expect(result[1]).to.be.equal("Foo");
  });
  it('nested capturing group nested with an "or" added', function(){
    var e = new engexp_1["default"]()
      .match('!')
      .beginCapture()
      .beginCapture()
      .match('Foo')
      .or('Bar')
      .endCapture()
      .oneOrMore()
      .endCapture()
      .match('$')
      .asRegExp();

    var result = e.exec('!FooBar$');
    chai_1.expect(result[0]).to.be.equal('!FooBar$');
    chai_1.expect(result[1]).to.be.equal('FooBar');
    chai_1.expect(result[2]).to.be.equal('Bar');
  });
});
